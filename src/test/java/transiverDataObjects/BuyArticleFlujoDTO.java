package transiverDataObjects;

/**
 * Objeto para transportar los datos de un escenario específico entre los distintos procesos.
 * @author Jean Guerra
 * @version $I$, $G$
 * @since 1.0
 */
public class BuyArticleFlujoDTO
{
	/**
	 * Metodo para cargar datos del excel
	 * @param oRow
	 * @throws Exception 
	 */
	public BuyArticleFlujoDTO(final Object[] oRow) throws Exception
	{
		setTestCase(oRow[0]);
		setBrowser(oRow[1]);
		setUser(oRow[2]);
		setPass(oRow[3]);
		//setarticle(oRow[4]);
		
	}

	private int testcase;
	public final int getTestCase()
	{
		return testcase;
	}
	
	final void setTestCase(Object testcase)
	{
		this.testcase = Integer.parseInt(testcase.toString().trim());
	}

	private String browser;
	public final String getBrowser()
	{
		return browser;
	}
	
	final void setBrowser(Object browser)
	{
		String temp = browser.toString().trim().toLowerCase();
		this.browser = temp;
	}

	private String user;
	public final String getUser()
	{
		return user;
	}

	public final void setUser(Object user)
	{
		this.user = user.toString().trim();
	}

    private String pass;
	public final String getPass()
	{
		return pass ;
	}

	final void setPass(Object pass)
	{
		this.pass = pass.toString().trim();
	}
	
	private String article;
	public final String getarticle()
	{
		return article;
	}

	public final void setarticle(Object artic)
	{
		this.article = artic.toString().trim();
	}

	
	
}