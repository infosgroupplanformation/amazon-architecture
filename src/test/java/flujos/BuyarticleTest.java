package flujos;

import transiverDataObjects.*;
import setUp.*;

import java.io.File;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.openqa.selenium.WebDriver;

import pagesFactory.ChooseArticlePom;
import pagesFactory.CommonPom;
import pagesFactory.DeliveryPom;
import pagesFactory.FindArticlePom;
import pagesFactory.ValidateFormLoginPom;
import pagesFactory.ShopcartPom;


/**
 * @author Scarling Pinto
 * @version $I$, $G$
 * @since 1.0
 */
@RunWith(Parameterized.class)
public class BuyarticleTest
{
	static String environmentVariable;
	static String dataTablePath;
	static String resultsPath;
	
	static Report report;
	static Files file;
	static Object[] data;
	
	static int ini;
	static int fin;
	static int counter;
	
/**
 * 
 * @param data
 * @throws Exception
 */
	public BuyarticleTest(Object[] data) throws Exception
	{
		BuyarticleTest.data = data;
	}
	
	@Parameterized.Parameters
	public static Object[] Data() throws Throwable
	{
		ini = Integer.valueOf(ConfigProperties.Get("StartRow"));
		//String os = System.getProperty("os.name").toLowerCase();

		if(ini < 1)
		{
			ini = 1;
		}
		counter = ini;
		
		fin = Integer.valueOf(ConfigProperties.Get("EndRow"));
		file = new Files();
		environmentVariable = ConfigProperties.getEnviromentPath(); // ->/Users/VictorHugo/INFOS/WebCheckIn/ or 
		System.out.println("environmentVariable:" + environmentVariable);
		dataTablePath = ConfigProperties.getPathOf("data_table");
		System.out.println("DataTable:" + dataTablePath);
		resultsPath = File.separator+file.getReportPath();
		System.out.println("resultsPath:" + resultsPath);
		report = new Report(environmentVariable, resultsPath, ini);
		file.createExcel(environmentVariable + resultsPath + File.separator + "EjemploFlujo results.xlsx", new BuyArticleResultsTable().getTable());
		
		Object[][] data = file.getData(environmentVariable + dataTablePath);
		if(fin > data.length)
		{
			fin = data.length;
		}
		return convertToJUnitArray(data);
	}
	
	/**
	 * Permite que el constructor reciba la fila como un array y no como elementos individuales. 
	 * Además filtra la cantidad de tests a ejecutar.
	 * @param biarray
	 * @return
	 */
	private static Object[] convertToJUnitArray(Object[][] biarray)
	{
		Object[] triarray = new Object[(fin-ini) + 1];
		
		for (int i=0; i<(fin-ini) + 1; i++)
		{
			triarray[i] = new Object[] { biarray[(ini-1)+i] };
		}
		
		return triarray;
	}

	@Test
	public void test() throws Throwable 
	{
		// reemplazar counter por dto.getTestCase()
		System.out.println("Initializing TC #" + counter); 
		WebDriver driver = null;
		BuyArticleFlujoDTO dto = null;
		
		try
		{
			try
			{
				dto = new BuyArticleFlujoDTO(data);
				Driver driverConfig = new Driver(dto.getBrowser());
				driver = driverConfig.getDriver();
			}
			catch(Exception e)
			{
				// reemplazar counter por dto.getTestCase()
				String testDescription = "<b>" + "Prueba #</b>" + counter + "<br>"; 
				testDescription += "<b>" + " SETEO DE DATOS FALLIDO " + "</b>";
				report.startTest(testDescription, null, null);
				report.testFail(e.getMessage());
				throw e;
			}
			// reemplazar counter por dto.getTestCase()
			String testDescription = "<b>" + "Prueba #" + counter + "</b>" + "<br>"; 
			testDescription += " Navegador: " + "<b>" + dto.getBrowser() + "</b>" + "<br>";
			
			
			
			report.startTest(testDescription, driver, dto.getBrowser());
			
			
			/*** INICIO DE PRUEBA ***/
			
			
			
			CommonPom commonPom = new CommonPom(driver, report);
			DeliveryPom delivery = new DeliveryPom(driver, report);
			FindArticlePom fa = new FindArticlePom(driver,report);
			ShopcartPom shop = new ShopcartPom(driver,report);
			ChooseArticlePom cap = new ChooseArticlePom(driver, report);
			
			commonPom.gotoamazon();
			commonPom.checkIfIsLoaded();
			//commonPom.clickOnStoreFront();
			//commonPom.checkSFrontAndLangLists();
			
			//commonPom.LoginInputData(dto.getUser(), dto.getPass());
			//fa.findArticleData(dto.getarticle());
			cap.clickOnDepartmentList();
			cap.clickOnItemList();
			shop.pickItems();
			shop.addToShoppingCart();
			
			
			//delivery.ProceedCheckout();
			//commonPom.Logout();
			
			//Findarticle fa = new Findarticle();

	
		
			//Nombre del objeto y llamada al método
			
			
			//Nombre del objeto y llamada al método
			
			
			
			//Nombre del objeto y llamada al método
			
			
			

			/*** FIN DE PRUEBA ***/
			
			
			counter++;
			
			report.endTest();

			driver.quit();
			
		}catch (Exception e)
		{
			counter++;
			
			if(driver != null)
			{
				driver.quit();
			}

			report.endTest();
			// file.writeData(data[i], "fail");
			e.printStackTrace();
			
			throw e;
		}

}}