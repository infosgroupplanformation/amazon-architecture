package pagesFactory;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import setUp.Report;

public class ShopcartPom {
	WebDriver driver;
	Report report;
	
	
	//**************Declaracion de String y WebElement*******************//
	
	
	@FindBy(xpath="(//div[@class='a-section a-spacing-none']/h2/child::*)[1]")
	private WebElement pickItem;
	
	@FindBy(xpath="//input[@name= 'submit.add-to-cart']")
	private WebElement addShoppingCart;

	public ShopcartPom(WebDriver driver, Report report){
		
		 	this.driver = driver;
	        this.report = report;
	        
	        //This initElements method will create all WebElements
	        
			PageFactory.initElements(driver, this);
	}
	
	
	public void pickItems() throws Exception {

		try{
			pickItem.click();
			report.testPass("Se elige el producto", "Elegir_Item", "Amazon", false);
			
		}catch (Throwable t){
			report.testFail("No se hace click en el producto", "Elegir_Item", "Amazon", false);
			throw t;
			
		}
				
	}
	
	public void addToShoppingCart() throws Exception{
		try{
			addShoppingCart.click();
			Thread.sleep(5000);
			report.testPass("Se añade el producto al carrito", "Añadir al carrito", "Amazon_Comprar", false);
			
		}catch (Throwable t){
			report.testFail("No se añade el producto", "Añadir al carrito", "Amazon_Comprar", false);
			throw t;
			
		}
			
	}
	
	private void scrollTowebelement(WebElement element) throws InterruptedException
	{
		JavascriptExecutor javaScriptExecutor = (JavascriptExecutor) driver;
		String scrollElementIntoMiddle = "var viewPortHeight = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);"
                + "var elementTop = arguments[0].getBoundingClientRect().top;"
                + "window.scrollBy(0, elementTop-(viewPortHeight/2));";
		javaScriptExecutor.executeScript(scrollElementIntoMiddle, element);
	}
	
	
	
	
	
	
}
