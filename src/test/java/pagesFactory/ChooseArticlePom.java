package pagesFactory;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import setUp.Report;

public class ChooseArticlePom {
	WebDriver driver;
	Report report;
	
	@FindBy(xpath="//div[@id='nav-shop']/a[contains(@class,'nav-a')]")
	private WebElement departmentList;
	
	@FindBy(xpath="//div[@class= 'nav-catFlyout nav-flyout']/div/a[contains(@class, 'nav-link')][10]")
	private WebElement computerDepartment;
	
	@FindBy(xpath="//a[@class= 'nav-logo-link']")
	private WebElement home;
	
	@FindBy(xpath="(//span[@class='a-list-item']/a/child::*)[1]")
	private WebElement itemList;
	
	
	public ChooseArticlePom(WebDriver driver, Report report) {
		this.driver = driver;
        this.report = report;
        
        //This initElements method will create all WebElements
        
		PageFactory.initElements(driver, this);
		
	}
	
	public void clickOnDepartmentList() throws Exception {
		home.click();
		
		WebDriverWait wait = new WebDriverWait(driver,3000);
		wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//div[@id='nav-shop']/a[contains(@class,'nav-a')]")));	
		//System.out.println(theader);
		
		try{
			scrollTowebelement(departmentList);
			Actions action = new Actions(driver);
			Thread.sleep(1500);
			
			//List<WebElement> listItems =driver.findElements(By.xpath("//div[contains(@class,'nav-template nav-flyout-content nav-tpl-itemList')]/a[contains(@class,'nav-link')]"));
			//listItems =driver.findElements(By.xpath("//div[contains(@class,'nav-template nav-flyout-content nav-tpl-itemList')]/a[contains(@class,'nav-link')]"));
			//La linea de codigo anterior  llena una lista de un encabezado y puede recorrerlo mediante un Switch Case

			action.moveToElement(departmentList).perform();
			report.testPass("Se hace click en la lista de departamentos", "Click Departamento", "Amazon_Click1", false);
			computerDepartment.click();
			
			
		}catch (Throwable t){
			report.testFail("No se hace click en la lista de departamentos", "Click Departamento", "Amazon_Click1", false);
			throw t;
			
		}	
	}

	
	public void clickOnItemList() throws Exception {
		try{
		WebDriverWait wait = new WebDriverWait(driver,3000);
		wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("(//span[@class='a-list-item']/a/child::*)[1]")));	
		report.testPass("Se hace click en la opcion de la lista ACCESSORIOS PARA COMPUTADORA", "Click en Lista", "Amazon_Click2", false);
		itemList.click();
		
		}catch (Throwable t){
			report.testFail("No se hace click en la opcion de la lista ACCESSORIOS PARA COMPUTADORA", "Click en Lista", "Amazon_Click2", false);
			throw t;
			
		}	
	}
	
	
	
	private void scrollTowebelement(WebElement element) throws InterruptedException
	{
		JavascriptExecutor javaScriptExecutor = (JavascriptExecutor) driver;
		String scrollElementIntoMiddle = "var viewPortHeight = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);"
                + "var elementTop = arguments[0].getBoundingClientRect().top;"
                + "window.scrollBy(0, elementTop-(viewPortHeight/2));";
		javaScriptExecutor.executeScript(scrollElementIntoMiddle, element);
	}
	
	
	
	
}
