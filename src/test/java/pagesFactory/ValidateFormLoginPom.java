package pagesFactory;


import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import flujos.BuyarticleTest;
import setUp.Report;

public class ValidateFormLoginPom
{
	
	
	WebDriver driver;
	Report report;
	
	@FindBy(xpath="//input[@type='email']")
	private WebElement wuser;
	
	@FindBy(xpath="//input[@type='password']")
	private WebElement wpassword;
	
	@FindBy(xpath="(//div[@class='nav-right']/div[@id='nav-tools']/child::*)[2]")
	private WebElement woptionlogin;
	
	@FindBy(xpath="//input[@class='a-button-input']")
	private WebElement wsend;
	
	
	
	
	
	
	/* Declarar las variables STRING de nuestros XPATH */
	
	
	
	
	

	public ValidateFormLoginPom(WebDriver driver, Report report)
	{
        this.driver = driver;
        this.report = report;
        
        //This initElements method will create all WebElements
        
		PageFactory.initElements(driver, this);
	}
	
		
	/**
	 * Se validan los elementos principales del formulario del Login
	 * 
	 *  @throws Exception
	 */
	
	public void CheckForm() throws Exception {
		
		
		//presencia de los elementos dentro de ese formulario
		
		
		try {
			
			new WebDriverWait(driver, 10).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@type='email']")));
			new WebDriverWait(driver, 10).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@type='password']")));
			new WebDriverWait(driver, 10).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@class='a-button-input']")));
			report.testPass("Se detecto formato valido","LoginInfoPage", "invalidDate_", false);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			report.testFail("Se detecto formato invalido","LoginInfoPage", "foto1", false);
			e.printStackTrace();

		}
		
	}//fin metodo
	

	

	
	


	
private void scrollTowebelement(WebElement element) throws InterruptedException
	{
		JavascriptExecutor javaScriptExecutor = (JavascriptExecutor) driver;
		String scrollElementIntoMiddle = "var viewPortHeight = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);"
                + "var elementTop = arguments[0].getBoundingClientRect().top;"
                + "window.scrollBy(0, elementTop-(viewPortHeight/2));";
		javaScriptExecutor.executeScript(scrollElementIntoMiddle, element);
	}
	
	
}
