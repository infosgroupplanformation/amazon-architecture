package pagesFactory;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import setUp.Report;

public class DeliveryPom {

	WebDriver driver;
	Report report;
	
	@FindBy(xpath="//a[contains(@href,'/gp/cart/view')]")
	private WebElement wgocar;
	
	@FindBy(xpath="//input[@name='proceedToCheckout']")
	private WebElement wgoform;
	
	@FindBy(xpath="//input[@type='text']")
	private WebElement wfillform;
	
	@FindBy(xpath="//select[contains(@class,'enterAddress')]/option[@value='PA']")
	private WebElement wspais;
	
	@FindBy(xpath="//input[@type='submit']")
	private WebElement wbtndeli;
	
	
	public DeliveryPom(WebDriver driver, Report report)
	{
        this.driver = driver;
        this.report = report;
        
        //This initElements method will create all WebElements
        
		PageFactory.initElements(driver, this);
	}
	
	
	public void ProceedCheckout()
	{
		wgocar.click();
		wgoform.click();
		wfillform.findElement(By.xpath("//input[@name='enterAddressFullName']")).sendKeys("");
		wfillform.findElement(By.xpath("//input[@name='enterAddressAddressLine1']")).sendKeys("Otro Texto de Prueba");
		wfillform.findElement(By.xpath("//input[@name='enterAddressAddressLine2']")).sendKeys("Otro Texto de Prueba");
		wfillform.findElement(By.xpath("//input[@name='enterAddressCity']")).sendKeys("Panamá");
		wfillform.findElement(By.xpath("//input[@name='enterAddressStateOrRegion']")).sendKeys("Panamá");
		wfillform.findElement(By.xpath("//input[@name='enterAddressPostalCode']")).sendKeys("0824");
		wspais.click();
		wfillform.findElement(By.xpath("//input[@name='enterAddressPhoneNumber']")).sendKeys("60821323");
		wfillform.findElement(By.xpath("//textarea[@name='AddressInstructions']")).sendKeys("Otro texto de Prueba");
		wfillform.findElement(By.xpath("//input[@name='GateCode']")).sendKeys("1234");
		wbtndeli.click();
	}
	
}
