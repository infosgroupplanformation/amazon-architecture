package pagesFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import setUp.Report;

/**
 * @author Automation team
 * @version $I$, $G$
 * @since 1.0
 */
public class CommonPom
{
	WebDriver driver;
	Report report;
	
	// Bitdefender
	@FindBy(xpath="")
	private WebElement EjemploDeWebElement1;
	
	// HTML lang
	@FindBy(xpath="//span[contains(@class, 'icp-nav-language')]")
	private WebElement weHtmlLang;
	
	// Header
	@FindBy(xpath="//div[@id='container-menu']")
	private WebElement weHeader;
	
	// Travel
	@FindBy(xpath="(//ul[@id='parentList']/descendant::a/span)[1]")
	private WebElement weTravel;
		
	// Plan
	@FindBy(xpath="(//ul[@id='parentList']/descendant::a/span)[2]")
	private WebElement wePlan;
		
	// Services
	@FindBy(xpath="(//ul[@id='parentList']/descendant::a/span)[3]")
	private WebElement weServices;
		
	// ConnectMiles
	@FindBy(xpath="(//ul[@id='parentList']/descendant::a/span)[4]")
	private WebElement weConnectMiles;
	
	// Ask Ana
	@FindBy(xpath="//button[@id='ask_ana_button']")
	private WebElement weAskAna;
	
	// Store front box
	@FindBy(xpath="//button[@id='id_store-front-btn']")
	private WebElement weStoreFrontBox;
	
	// Store front
	@FindBy(xpath="//button[@id='storefront_select-country']")
	private WebElement weStoreFront;
	
	// Lang
	@FindBy(xpath="//button[@id='storefront_select-lang']")
	private WebElement weLang;
	
	// Accept
	@FindBy(xpath="//a[@id='storefront_accept']")
	private WebElement weAccept;
	

	//amazon Login
	
	@FindBy(xpath="//input[@type='email']")
	private WebElement wuser;
	
	@FindBy(xpath="//input[@type='password']")
	private WebElement wpassword;
	
	@FindBy(xpath="(//div[@class='nav-right']/div[@id='nav-tools']/child::*)[2]")
	private WebElement woptionlogin;
	
	@FindBy(xpath="//input[@class='a-button-input']")
	private WebElement wsend;
	
	
	//Amazon Logout
	
	
	@FindBy(xpath="//a[contains(@id,'nav-item-signout')]")
	private WebElement wlogout;
	
	@FindBy(xpath="//a[contains(@data-nav-role,'signin') and not(@rel)]")
	private WebElement wmenu;
	
	

	
	
	
	public CommonPom(WebDriver driver, Report report)
	{
        this.driver = driver;
        this.report = report;
        
        //This initElements method will create all WebElements
        
		PageFactory.initElements(driver, this);
	}
	
	/**
	 * Ingresar a:
	 * https://amazon.com.
	 * @throws Exception
	 */
	public void gotoamazon() throws Exception
	{
		try
		{
			driver.navigate().to("https://www.amazon.com/");
			closeBitdefenderWindow();
			Thread.sleep(6000);
			report.testPass("Ingresar a Amazon", "Homepage", "Main", true); //, "merge login/positive", "Página inicial", false);
		}
		catch(Throwable t)
		{
			report.testPass("No se pudo ingresar a Amazon", "Homepage", "Main", true);
			throw t;
		}
	}
	
	private void closeBitdefenderWindow() throws Exception
	{
		try
		{
			EjemploDeWebElement1.click();
		}
		catch(Throwable t)
		{
			// De no mostrarse la pantalla del firewall, continuar con normalidad.
		}
	}

	/**
	 * La página carga el header con sus elementos:
	 * 
	 * Y que se mantenga en el enlace indicado.
	 * @throws Exception
	 */
	public void checkIfIsLoaded() throws Exception
	{
		try
		{
			Thread.sleep(7000);
			List<Boolean> checklist = new ArrayList<Boolean>();
			checklist.add(wmenu.isDisplayed());
			
			String lang = weHtmlLang.getText().substring(0, 2);
			
			switch(lang.toLowerCase())
			{
				case "es":
					checklist.add(woptionlogin.getText().toLowerCase().equals("Identifícate"));
				/*
				 * checklist.add(wePlan.getText().toLowerCase().equals("planificar"));
				 * checklist.add(weServices.getText().toLowerCase().equals("servicios"));
				 * checklist.add(weConnectMiles.getText().toLowerCase().equals("connectmiles"));
				 */
					break;
				case "en":
					checklist.add(woptionlogin.getText().toLowerCase().equals("Sign in"));
				/*
				 * checklist.add(wePlan.getText().toLowerCase().equals("plan"));
				 * checklist.add(weServices.getText().toLowerCase().equals("services"));
				 * checklist.add(weConnectMiles.getText().toLowerCase().equals("connectmiles"));
				 */
					break;
				case "pt":
					checklist.add(weTravel.getText().toLowerCase().equals("viajar"));
					checklist.add(wePlan.getText().toLowerCase().equals("planejar"));
					checklist.add(weServices.getText().toLowerCase().equals("serviços"));
					checklist.add(weConnectMiles.getText().toLowerCase().equals("connectmiles"));
					break;
				default:
					throw new Exception("Idioma no implementado.");
			}
			
			//checklist.add(weAskAna.isDisplayed());
			checklist.add(driver.getCurrentUrl().contains("amazon.com"));
			
			/*
			 * for (Boolean item : checklist) { if (item == false) { throw new
			 * Exception(Arrays.toString(checklist.toArray())); } }
			 */
			
			report.testPass("Ingresar a Amazon", "Homepage", "Main", true); 
		}
		catch(Throwable t)
		{
			report.testPass("No se pudo ingresar a Amazon", "Homepage", "Main", true);
			throw t;
		}
	}

	/**
	 * Desplegar recuadro de Storefront.
	 * @throws Exception
	 */
	public void clickOnStoreFront() throws Exception
	{
		try
		{
			weStoreFrontBox.click();
			report.testPass("Desplegar recuadro de Storefront."); // , "merge login/positive", "Store front box", false);
		}
		catch(Throwable t)
		{
			report.testFail("Desplegar recuadro de Storefront.");
			throw t;
		}
	}

	/**
	 * El recuadro debe mostrar una lista para elegir país y otra para idioma, y botón de aceptar.
	 * @throws Exception
	 */
	public void checkSFrontAndLangLists() throws Exception
	{
		try
		{
			weStoreFront.isDisplayed();
			weLang.isDisplayed();
			weAccept.isDisplayed();
			report.testPass("El recuadro debe mostrar una lista para elegir país y otra para idioma, y botón de aceptar.", "merge login/positive", "Controles de store front", false);
		}
		catch(Throwable t)
		{
			report.testFail("El recuadro debe mostrar una lista para elegir país y otra para idioma, y botón de aceptar.", "merge login/positive", "Controles de store front", true);
			throw t;
		}
	}

	/**
	 * Seleccionar Storefront e Idioma de acuerdo a la hoja de cálculo.
	 * @param lang
	 * @param storeFront
	 * @throws Exception
	 */
	public void selectStoreFront(String storeFront, String lang) throws Exception
	{
		try
		{
			weStoreFront.click();
			driver.findElement(By.xpath("//a[@id='country_" + storeFront.toUpperCase() + "']")).click();

			weLang.click();
			driver.findElement(By.xpath("//a[@id='language_" + lang.toLowerCase() + "']")).click();
			
			report.testPass("Seleccionar Storefront e Idioma de acuerdo a la hoja de cálculo."); // , "merge login/positive", "Selección de store front", false);
		}
		catch(Throwable t)
		{
			report.testFail("Seleccionar Storefront e Idioma de acuerdo a la hoja de cálculo.");
			throw t;
		}
	}

	/**
	 * En la lista desplegable se muestre el país e idioma seleccionado.
	 * @throws Exception
	 */
	public void checkEnteredValues(String storeFront, String lang) throws Exception
	{
		try
		{
	        //This initElements method will create all WebElements
	        
			PageFactory.initElements(driver, this);
			
			String selectedStoreFront = weStoreFront.getText();
			WebElement weSelectedStoreFront = driver.findElement(By.xpath("//a[contains(text(), '" + selectedStoreFront + "')]"));
			String weStoreFrontTxt = weSelectedStoreFront.getAttribute("id");

			String selectedLang = weLang.getText();
			WebElement weSelectedLang = driver.findElement(By.xpath("//a[contains(text(), '" + selectedLang + "')]"));
			String weLangTxt = weSelectedLang.getAttribute("id");
			
			if(weStoreFrontTxt.contains(storeFront.toUpperCase()) && weLangTxt.contains(lang.toLowerCase()))
			{
				report.testPass("En la lista desplegable se muestre el país e idioma seleccionado.", "merge login/positive", "Valores de store front", false);
			}
			else
			{
				throw new Exception();
			}
		}
		catch(Throwable t)
		{
			report.testFail("En la lista desplegable se muestre el país e idioma seleccionado.", "merge login/positive", "Valores de store front", true);
			throw t;
		}
	}

	/**
	 * Clic en aceptar.
	 * @throws Exception
	 */
	public void clickOnAccept() throws Exception
	{
		try
		{
			weAccept.click();
			report.testPass("Clic en aceptar."); // , "merge login/positive", "Aceptar", false);
		}
		catch(Throwable t)
		{
			report.testFail("Clic en aceptar.");
			throw t;
		}
	}

	
	
	/**
	 * Se  inicia sesión en el sitio Web.
	 * @throws Exception
	 */
	public void LoginInputData( String user, String password) throws Exception {
		
		ValidateFormLoginPom login= new ValidateFormLoginPom(driver, report);
		
		
		woptionlogin.click();
		login.CheckForm();
		wuser.sendKeys(user);
		wpassword.sendKeys(password);
		wsend.click();
		report.testPass("Se hace login exitosamente","LoginInfoPage", "foto2", false);
		Thread.sleep(2000);
		
		
		
		
	}
	
	/**
	 * Se  cierra  sesión en el sitio Web.
	 * @throws Exception
	 */
	
	public void Logout() throws Exception
	{

		try {
			
			Actions action = new Actions(driver);
			action.moveToElement(wmenu).build().perform();
			report.testPass("Se despliega menú Login en Amazon","LoginMenu", "DesplayMenuLogin", false);
			wlogout.click();
			report.testPass("Se cierra sesión en Amazon","LoginInfoPage", "LoginForm", false);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			report.testFail("No se pudo cerrar la sesión en Amazon","LoginInfoPage", "LoginForm", false);
			e.printStackTrace();

		}
		
	}
	
	/**
	 * La página reedirija a https://amazon.com/[LANG]/web/[STOREFRONT].
	 * @throws Exception
	 */
	
	public void checkRedirection(String storeFront, String lang) throws Exception
	{
		try
		{	
			if(driver.getCurrentUrl().contains("amazon.com/" + lang.toLowerCase() + "/web/" + storeFront.toLowerCase()))
			{
				report.testPass("La página reedirija a https://amazon.com/" + lang.toLowerCase() + "/web/" + storeFront.toLowerCase() + ".", "merge login/positive", "Redirección", true);
			}
			else
			{
				throw new Exception();
			}
		}
		catch(Throwable t)
		{
			report.testFail("La página reedirija a https:/amazon.com/" + lang.toLowerCase() + "/web/" + storeFront.toLowerCase() + ".", "merge login/positive", "Redirección", true);
			throw t;
		}
	}
}
