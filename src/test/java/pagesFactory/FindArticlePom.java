package pagesFactory;



import setUp.Report;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class FindArticlePom {

	
	WebDriver driver;
	Report report;
	
	/* Declarar las variables STRING de nuestros XPATH */
	
	@FindBy(xpath="//input[@type='text']")
	private WebElement barra;
	
	@FindBy(xpath="//div[@class='nav-search-submit nav-sprite']/child::input")
	private WebElement btn_buscar;
	
	
	public FindArticlePom(WebDriver driver, Report report)
	{
        this.driver = driver;
        this.report = report;
        
        //This initElements method will create all WebElements
        
		PageFactory.initElements(driver, this);
	}
	

	
		
	public void findArticleData( String article) throws Exception {
		
		try {
		
		barra.click();
		barra.sendKeys(article);
		btn_buscar.click();
		report.testPass("Se ingresa el nombre del articulo","FindArticle", article , true);
		Thread.sleep(2000);
		}catch (Exception e){
			
			report.testFail("No se ingresa el nombre del articulo", "Findarticle", article, false);
			e.printStackTrace();
		}
		
	} 
	
	
	
	
	
	private void scrollTowebelement(WebElement element) throws InterruptedException
	{
		JavascriptExecutor javaScriptExecutor = (JavascriptExecutor) driver;
		String scrollElementIntoMiddle = "var viewPortHeight = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);"
                + "var elementTop = arguments[0].getBoundingClientRect().top;"
                + "window.scrollBy(0, elementTop-(viewPortHeight/2));";
		javaScriptExecutor.executeScript(scrollElementIntoMiddle, element);
	}
		
	}//fin metodo


	
