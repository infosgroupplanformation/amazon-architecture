package setUp;

import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.shooting.ShootingStrategies;

/**
 * @author Jean Guerra
 * @version $I$, $G$
 * @since 1.0
 */
public final class Report
{
	private ExtentReports extent;
	private ExtentTest test;
	private String reportPath;
	private int counter;
	private WebDriver driver;
	private String browser;
	private String os = System.getProperty("os.name").toLowerCase();

	/**
	 * Crea un nuevo reporte de ExtentReports en la ruta especificada.
	 * @param environmentPath Ruta en la que está el ambiente.
	 * @param resultsPath Ruta en la que será almacenado el reporte.
	 * @param counter Contador del reporte.
	 * @throws IOException
	 */
	public Report(String environmentPath, String resultsPath, int counter) throws IOException
	{
		reportPath = environmentPath + resultsPath;
		this.counter = counter;
		String reportFilePath;
		reportFilePath = environmentPath + resultsPath + File.separator +"report.html";

		new File(environmentPath + resultsPath).mkdirs();
		new File(reportFilePath).createNewFile();
		
		this.extent = new ExtentReports(reportFilePath, true);
		//String configFilePath = environmentPath + "\\reportconfig.xml";
		String configFilePath;
		configFilePath = environmentPath + File.separator + "reportconfig.xml";
		extent.loadConfig(new File(configFilePath));
	}
	
	/**
	 * 
	 * @param name Título del test.
	 * @param driver WebDriver utilizado para cuando hay que hacer logs con imágenes.
	 * @param browser Permite que pueda omitirse (para el caso de IE) el tipo de foto que no es de la página entera.
	 */
	public void startTest(String name, WebDriver driver, String browser)
	{
		this.test = extent.startTest(name);
		this.driver = driver;
		this.browser = browser;
	}
	
	public void testPass(String description)
	{
		this.test.log(LogStatus.PASS, description);
	}
	
	public void testPass(String description, String path, String name, boolean isFullSize) throws Exception
	{
		try
		{
			testWithCapture(description, path, name, isFullSize, LogStatus.PASS);
		}
		catch(Exception e)
		{
			this.test.log(LogStatus.FAIL, "Error irrecuperable del framework, el test no concluirá correctamente.");
			throw e;
		}
	}
	
	public void testWarning(String description)
	{
		this.test.log(LogStatus.WARNING, description);
	}
	
	public void testWarning(String description, String path, String name, boolean isFullSize) throws Exception
	{
		try
		{
			testWithCapture(description, path, name, isFullSize, LogStatus.WARNING);
		}
		catch(Exception e)
		{
			this.test.log(LogStatus.FAIL, "Error irrecuperable del framework, el test no concluirá correctamente.");
			throw e;
		}
	}
	
	public void testFail(String description)
	{
		this.test.log(LogStatus.FAIL, description);
	}
	
	public void testFail(String description, String path, String name, boolean isFullSize) throws Exception
	{
		try
		{
			testWithCapture(description, path, name, isFullSize, LogStatus.FAIL);
		}
		catch(Exception e)
		{
			this.test.log(LogStatus.FAIL, "Error irrecuperable del framework, el test no concluirá correctamente.");
			throw e;
		}
	}
	
	/**
	 * Genera un log con imagen adjunta.
	 * @param description Mensaje a mostrar.
	 * @param path Ruta de la imagen. No debe empezar con "\\".
	 * @param name Nombre de la imagen.
	 * @param isFullSize Indica si debe tomarse foto de la página entera.
	 * @param logStatus Estado de log.
	 * @throws IOException
	 */
	private void testWithCapture(String description, String path, String name, boolean isFullSize, LogStatus logStatus) throws Exception
	{	
		name += ".png";
		String imgPath;
		String imgFullPath;
		
		if (os.contains("mac"))
		{
			imgPath = "test " + counter + "/" + path;
			imgFullPath = reportPath + "/" + imgPath;
		} 
		else 
		{
			imgPath = "test " + counter + "\\" + path;
			imgFullPath = reportPath + "\\" + imgPath;
		}
		

		new File(imgFullPath).mkdirs();
		//File file = new File(imgFullPath + "\\" + name);
		File file;
		if (os.contains("mac"))
		{
			file = new File(imgFullPath + "/" + name);
		} 
		else 
		{
			file = new File(imgFullPath + "\\" + name);
		}
		file.createNewFile();
		
		if(isFullSize && !browser.equalsIgnoreCase("InternetExplorer"))
		{
			 if(browser.equalsIgnoreCase("MozilaFireFox"))
			 {
				 Thread.sleep(5000); // Las nuevas versiones de FireFox generan problemas si no se coloca este tiempo.
			 }
			
			ImageIO.write(new AShot().shootingStrategy(ShootingStrategies.viewportPasting(100)).takeScreenshot(driver).getImage(), "PNG", file);
		}
		else
		{
			// Método más rápido en IE
			FileUtils.copyFile(((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE), file);
		}
		
		//String html = test.addScreenCapture(imgPath + "\\" + name);
		String html;
		if (os.contains("mac"))
		{
			html = test.addScreenCapture(imgPath + "/" + name);
		} 
		else 
		{
			html = test.addScreenCapture(imgPath + "\\" + name);
		}
		this.test.log(logStatus, description + "<br>" + html);
	}
	
	public void endTest()
	{
		extent.endTest(test);
		extent.flush();
		counter++;
	}
	
	public void endReport()
	{
		extent.flush();
		extent.close();
	}
}