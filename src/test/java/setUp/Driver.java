package setUp;
import java.util.concurrent.TimeUnit;
import org.apache.commons.lang3.SystemUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions; 
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;



import io.github.bonigarcia.wdm.WebDriverManager;

/**
 * @author - InfosGroup - Jean Guerra
 * @version $I$, $G$
 * @since 1.0
 * Last Update: July 30, 2018 - victor Orrego
 * Add Cross-Platform functionality for Mac OS and Windows compatibility. 
 */
public final class Driver
{
	private String browser;
	//private WebDriver driver;
	
	public Driver(String browser) throws Exception
	{
		if(browser.equalsIgnoreCase("googlechrome") || browser.equalsIgnoreCase("mozilafirefox") || browser.equalsIgnoreCase("internetexplorer"))
		{
			this.browser = browser;
		}
		else
		{
			throw new Exception(browser + " no está implementado.");
		}
	}
	
	public WebDriver getDriver() throws Exception 
	{
		switch(browser.toLowerCase())
		{
		
			case "googlechrome":
				
		        WebDriverManager.chromedriver().setup();
				ChromeOptions options = new ChromeOptions();
				options.addArguments("--start-maximized");
				return new ChromeDriver(options);
				
			case "mozilafirefox":
				
				WebDriverManager.firefoxdriver().setup();
				FirefoxOptions caps = new FirefoxOptions();
				caps.setCapability("acceptInsecureCerts", true);
			
				
				return new FirefoxDriver(caps);
			
			case "internetexplorer":
				
				if(SystemUtils.IS_OS_WINDOWS_10)
				{
					WebDriverManager.iedriver().setup();
					return new InternetExplorerDriver();
				}
				else 
				{
					WebDriverManager.edgedriver().setup();
					return new EdgeDriver();
				}
				
			default:
				
				throw new Exception(browser + " no está implementado.");
	}

	}
	
	public void config(WebDriver driver)  throws Exception
	{
		long IMPLICITLY_WAIT_PAGE = Long.valueOf(ConfigProperties.Get("IMPLICITLY_WAIT_PAGE"));
		
		 driver.manage().deleteAllCookies();
		// driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(IMPLICITLY_WAIT_PAGE, TimeUnit.MINUTES);
		//driver.manage().timeouts().setScriptTimeout(30, TimeUnit.SECONDS);
		driver.manage().timeouts().setScriptTimeout(IMPLICITLY_WAIT_PAGE, TimeUnit.MINUTES);
	}
}