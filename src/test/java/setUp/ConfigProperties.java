package setUp;


import java.io.File;
//import java.nio.file.Files;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
//import java.util.StringJoiner;

//import java.io.BufferedReader;
//import java.io.InputStream;
//import java.io.FileNotFoundException;

public class ConfigProperties {
	//private static java.nio.file.Files javaFilesClass;
	static String projectName = "PlantillaAutomatización";
	static String config_file = "config.txt";
	
	/**
	 * Obtiene la ruta que contiene los data tables y reportes del proyecto. 
	 * @param dataTablePath
	 * @return Ruta del reporte /"user"/INFOS/"projectName"/
	 * @throws IOException 
	 */
	public static String getEnviromentPath() throws IOException 
	{
		String strPath = System.getProperty("user.home")+ File.separator + "INFOS"+ File.separator + projectName + File.separator;
		return strPath;
	}
	
	/**
	 * Obtiene la ruta del data table en el formato iOS o Windows. 
	 * @param Name of the path on the 
	 * @return path value 
	 * @throws IOException 
	 */
	public static String getPathOf(String path) throws IOException {
		String holder = Get(path);
		String strPath = path+ File.separator + holder;
		
		//Identify if it exists.
		File tmpDir = new File(getEnviromentPath() + strPath);
		//System.out.println("data table path: "+tmpDir.getAbsolutePath());
		boolean exists = tmpDir.exists();
		System.out.println(exists);
		 if (!exists) 
		    {
		    	System.out.println("creating a new data table:" + strPath );
		    	File datatable_default = new File("DefaultData/test.xlsx");
		    	FileUtils.copyFile(datatable_default, tmpDir);
		    	
		    	File tmpDir2 = new File(getEnviromentPath() + "reportconfig.xml");
		    	File reportconfig_default = new File("DefaultData/reportconfig.xml");
		    	FileUtils.copyFile(reportconfig_default, tmpDir2);
		    }
		return strPath;
	}
	
	/***
	 * Get value parameter from property name
	 * @param propertyName
	 * @return Property Value
	 * @throws IOException
	 */

	public static String  Get(String vpPropertyName) throws IOException 
	{
		String strPath = getEnviromentPath() + config_file;
		File tmpDir = new File(strPath); // usar otro metodo para no crear esta instancia
	    boolean exists = tmpDir.exists();
	    //si el config no existe, copiar el config default
	    if (!exists) 
	    {
	    	System.out.println("creating a new config.txt file at:" + strPath );
	    	File config_default = new File("DefaultData/config.txt");
	    	FileUtils.copyFile(config_default, tmpDir);
	    }
		
		String strPropertyValue = null;
		FileReader inputStream = new FileReader(new java.io.File(strPath));
		
		try {
			//Se verifica que existe el archivo en la ruta
			if (inputStream != null)
			{
				//Se cargan las propiedades
	        	Properties properties = new Properties();
	        	properties.load(inputStream);
	        	// Obtiene el valor de la propiedad
		        strPropertyValue = properties.getProperty(vpPropertyName);
		        }
	        /*else {
	        	//File directory = new File("./");
	        	//System.out.println(directory.getAbsolutePath());
	        	Properties properties = new Properties();
	        	properties.load(inputStream);
		        strPropertyValue = properties.getProperty(vpPropertyName);
			}*/
		 }
		 finally{
			 inputStream.close();
		 }
	        //retorno del valor
			//System.out.println("Get():" + strPropertyValue);
	        return strPropertyValue;
	    }
		
}
