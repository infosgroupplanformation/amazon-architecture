package setUp;

import java.io.File;

//import java.io.File;

//import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
//import java.io.FileReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont; 
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 * Clase que permite la gestión de archivos en general.
 * Last Update: July 31st, 2018.
 * Notes: Se integra el metodo createResultsFolder
 * @author Jean Guerra
 * @version $I$, $G$
 * @since 1.0
 */
public final class Files
{
	//private java.io.File javaFileClass;
	private int rowCount = 0;
	private String pnrPath;
	
	/**
	 * Obtiene la ruta de los reportes en formato iOS o Windows.
	 * @return path value from the config file
	 */
	public String getReportPath()  {
		String strPath = "Results"+ File.separator + currentTime();
		//System.out.println("getReportPath:" + strPath);
		return strPath;
	}
	
	
	

	/**
	 * Obtiene la fecha y hora actual en un formato compatible con
	 * los nombres que acepta el sistema operativo.
	 * @return Fecha y hora actual en un formato compatible con los
	 * nombres que acepta el sistema operativo.
	 */
	private String currentTime()
	{
		return new SimpleDateFormat("HH:mm:ss dd/MM/yyyy").format(new Date()).replace(":", ".").replace("/", "-");
	}
	
	/**
	 * Carga los datos a partir del datatable.
	 * @param xlsxPath Ruta del datatable.
	 * @return Matriz de datos cargados.
	 * @throws Exception
	 */
	public Object[][] getData(String xlsxPath) throws Exception
	{
		FileInputStream file = new FileInputStream(new java.io.File(xlsxPath));
		XSSFWorkbook workbook = new XSSFWorkbook(file);
		XSSFSheet sheet = workbook.getSheetAt(0);
		
		final int rowsSize = sheet.getLastRowNum();
		Object[][] data = new Object[rowsSize][];
		
		Iterator<Row> rowIterator = sheet.iterator();
		
		if(rowIterator.hasNext())
		{
			rowIterator.next(); // salta la primera fila
		}
		else
		{
			workbook.close();
			throw new Exception("La hoja de cálculo está vacía.");
		}

		int rowCounter = 0, columnCounter = 0;
		while (rowIterator.hasNext())
		{
			final int columnsSize = sheet.getRow(0).getLastCellNum();
			data[rowCounter] = new Object[columnsSize];	

			Row row = rowIterator.next();
			Iterator<Cell> cellIterator = row.cellIterator();
			
			while (cellIterator.hasNext())
			{
				Cell celda = cellIterator.next();
				
				try
				{
					try
					{
						data[rowCounter][columnCounter] = celda.getStringCellValue();
					}
					catch(Throwable error)
					{
						data[rowCounter][columnCounter] = String.valueOf((int)celda.getNumericCellValue());
					}
				}
				catch(Throwable error)
				{
					error.printStackTrace();
				}

				columnCounter++;
			}
			
			rowCounter++;
			columnCounter = 0;
		}
		
		workbook.close();
		file.close();
		return data;
	}
	
	public void createExcel(String xlsxPath, String tags[]) throws Exception
	{
		pnrPath = xlsxPath;
		
		XSSFWorkbook workbook = new XSSFWorkbook();
		XSSFSheet sheet = workbook.createSheet();
		
		XSSFFont defaultFont= workbook.createFont();
		defaultFont.setFontHeightInPoints((short) 15);
		defaultFont.setBold(true);
		XSSFCellStyle style = workbook.createCellStyle();
		style.setAlignment(HorizontalAlignment.CENTER);
		style.setFont(defaultFont);
		
		Row row = sheet.createRow(rowCount++);

		for (int i=0; i<tags.length; i++)
		{
			Cell cell = row.createCell(i);
			cell.setCellValue(tags[i]);
			cell.setCellStyle(style);
		}
		
		try (FileOutputStream outputStream = new FileOutputStream(xlsxPath))
		{
            workbook.write(outputStream);
            workbook.close();
        }
	}
	
	public void writeData(Object[] dto) throws Exception
	{
		FileInputStream file = new FileInputStream(new java.io.File(pnrPath));
		XSSFWorkbook workbook = new XSSFWorkbook(file);
		XSSFSheet sheet = workbook.getSheetAt(0);
		Row row = sheet.createRow(rowCount++);
		
		
		for (int i=0; i<dto.length; i++)
		{
			Cell cell = row.createCell(i); // Ya que la 0 es la de PNR
			cell.setCellValue((String) dto[i]);
			sheet.autoSizeColumn(i); // Ya que la 0 es la de PNR
		}

		try (FileOutputStream outputStream = new FileOutputStream(pnrPath))
		{
            workbook.write(outputStream);
            workbook.close();
        }
		
		file.close();
	}
}

